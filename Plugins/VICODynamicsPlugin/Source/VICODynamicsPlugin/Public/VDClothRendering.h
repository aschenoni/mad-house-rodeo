// Copyright 2012-2017 VICO Game Studio LLC. Rights Reserved.

#pragma once

#include "VDProceduralClothComponent.h"
#include "VDMeshClothComponent.h"
#include "DynamicMeshBuilder.h"
#include "PrimitiveSceneProxy.h"
#include "Materials/Material.h"
#include "MeshBatch.h"
#include "Engine/Engine.h"
#include "SceneManagement.h"

DECLARE_CYCLE_STAT(TEXT("ProcBuildClothMesh"), STAT_ProcBuildClothMesh, STATGROUP_VICODynamicsRendering);
DECLARE_CYCLE_STAT(TEXT("ProcClothSetDynamicDataRenderThread"), STAT_ProcClothSetDynamicDataRenderThread, STATGROUP_VICODynamicsRendering);
DECLARE_CYCLE_STAT(TEXT("ProcClothGetDynamicMeshElements"), STAT_ProcClothGetDynamicMeshElements, STATGROUP_VICODynamicsRendering);

/** Vertex Buffer */
class FVDClothVertexBuffer : public FVertexBuffer
{
public:
	virtual void InitRHI() override
	{
		FRHIResourceCreateInfo CreateInfo;
		VertexBufferRHI = RHICreateVertexBuffer(NumVerts * sizeof(FDynamicMeshVertex), BUF_Dynamic, CreateInfo);
	}

	int32 NumVerts;
};

/** Index Buffer */
class FVDClothIndexBuffer : public FIndexBuffer
{
public:
	virtual void InitRHI() override
	{
		FRHIResourceCreateInfo CreateInfo;
		IndexBufferRHI = RHICreateIndexBuffer(sizeof(int32), NumIndices * sizeof(int32), BUF_Dynamic, CreateInfo);
	}

	int32 NumIndices;
};

/** Vertex Factory */
class FVDClothVertexFactory : public FLocalVertexFactory
{
public:

	FVDClothVertexFactory()
	{}


	/** Initialization */
	void Init(const FVDClothVertexBuffer* VertexBuffer)
	{
		if (IsInRenderingThread())
		{
			// Initialize the vertex factory's stream components.
			FDataType NewData;
			NewData.PositionComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, Position, VET_Float3);
			NewData.TextureCoordinates.Add(
				FVertexStreamComponent(VertexBuffer, STRUCT_OFFSET(FDynamicMeshVertex, TextureCoordinate), sizeof(FDynamicMeshVertex), VET_Float2)
				);
			NewData.TangentBasisComponents[0] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, TangentX, VET_PackedNormal);
			NewData.TangentBasisComponents[1] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, TangentZ, VET_PackedNormal);
			SetData(NewData);
		}
		else
		{
			ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
				InitVDClothVertexFactory,
				FVDClothVertexFactory*, VertexFactory, this,
				const FVDClothVertexBuffer*, VertexBuffer, VertexBuffer,
				{
					// Initialize the vertex factory's stream components.
					FDataType NewData;
					NewData.PositionComponent = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, Position, VET_Float3);
					NewData.TextureCoordinates.Add(
						FVertexStreamComponent(VertexBuffer, STRUCT_OFFSET(FDynamicMeshVertex, TextureCoordinate), sizeof(FDynamicMeshVertex), VET_Float2)
						);
					NewData.TangentBasisComponents[0] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, TangentX, VET_PackedNormal);
					NewData.TangentBasisComponents[1] = STRUCTMEMBER_VERTEXSTREAMCOMPONENT(VertexBuffer, FDynamicMeshVertex, TangentZ, VET_PackedNormal);
					VertexFactory->SetData(NewData);
				});
		}
	}
};

/** Dynamic data sent to render thread */
struct FVDClothDynamicData
{
	/** Array of points */
	TArray<FVector> VDClothPoints;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	TArray<FVDDebugRenderData> DebugRenderData;
#endif

#if WITH_EDITORONLY_DATA
	TArray<FVector> PointsToHighlight;
#endif
};

//////////////////////////////////////////////////////////////////////////
// FVDProceduralClothSceneProxy

class FVDProcduralClothSceneProxy : public FPrimitiveSceneProxy
{
public:

	FVDProcduralClothSceneProxy(UVDProceduralClothComponent* Component)
		: FPrimitiveSceneProxy(Component)
		, Material(NULL)
		, DynamicData(NULL)
		, MaterialRelevance(Component->GetMaterialRelevance(GetScene().GetFeatureLevel()))
		, Width(Component->NumParticlesWide)
		, Height(Component->NumParticlesHigh)
		, TileMaterialAlongWidth(Component->TileMaterialAlongWidth)
		, TileMaterialAlongHeight(Component->TileMaterialAlongHeight)
		, TornPoints(Component->Tears)
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		, ShowParticles(Component->ShowParticles)
#endif
	{
		VertexBuffer.NumVerts = GetRequiredVertexCount();
		checkf(VertexBuffer.NumVerts, TEXT("Vertex Num is zero!"));

		IndexBuffer.NumIndices = GetRequiredIndexCount();
		checkf(IndexBuffer.NumIndices, TEXT("Index Num is zero!"));

		// Init vertex factory
		VertexFactory.Init(&VertexBuffer);

		// Enqueue initialization of render resource
		BeginInitResource(&VertexBuffer);
		BeginInitResource(&IndexBuffer);
		BeginInitResource(&VertexFactory);

		// Grab material
		Material = Component->GetMaterial(0);
		if (Material == NULL)
		{
			Material = UMaterial::GetDefaultMaterial(MD_Surface);
		}
	}

	virtual ~FVDProcduralClothSceneProxy()
	{
		VertexBuffer.ReleaseResource();
		IndexBuffer.ReleaseResource();
		VertexFactory.ReleaseResource();

		if (DynamicData != NULL)
		{
			delete DynamicData;
		}
	}

	int32 GetRequiredVertexCount() const
	{
		return Width * Height;
	}

	int32 GetRequiredIndexCount() const
	{
		return (((Width - 1) * (Height - 1)) * 3 * 2) - TornPoints.Num() * 3 * 2;
	}

	int32 GetVertIndex(int32 x, int32 y) const
	{
		return x + (y * Width);
	}

	void BuildClothMesh(const TArray<FVector>& InPoints, TArray<FDynamicMeshVertex>& OutVertices, TArray<int32>& OutIndices)
	{
		SCOPE_CYCLE_COUNTER(STAT_ProcBuildClothMesh);

		const FColor VertexColor(255, 255, 255);
		const int32 NumPoints = InPoints.Num();

		// Build vertices
		
		if (NumPoints < 2)
			return;

		for (int32 y = 0; y < Height; y++)
		{
			const float AlongY = (float) y / (float) Height; // Distance along VDCloth Height
									
			for (int32 x = 0; x < Width; x++)
			{
				const auto index = x + y * Width;
				const float AlongX = (float) x / (float) Width;
				
				FDynamicMeshVertex Vert;
				Vert.Position = InPoints[index];
				Vert.TextureCoordinate = FVector2D(AlongX * TileMaterialAlongWidth, AlongY * TileMaterialAlongHeight);
				Vert.Color = VertexColor;
				OutVertices.Add(Vert);
			}
		}

		// Build triangles
		for (int32 y = 0; y < Height - 1; y++)
		{
			for (int32 x = 0; x < Width - 1; x++)
			{
				const auto index = GetVertIndex(x, y);
				
				int32 TL = index;
				int32 BL = GetVertIndex(x, y + 1);
				int32 TR = GetVertIndex(x + 1, y);
				int32 BR = GetVertIndex(x + 1, y + 1);

				OutIndices.Add(TL);
				OutIndices.Add(BL);
				OutIndices.Add(TR);

				OutIndices.Add(TR);
				OutIndices.Add(BL);
				OutIndices.Add(BR);

				// Generate normals
				auto TriNormal1 = -((OutVertices[BL].Position - OutVertices[TL].Position) ^ (OutVertices[TR].Position - OutVertices[TL].Position));
				auto TriNormal2 = -((OutVertices[BL].Position - OutVertices[TR].Position) ^ (OutVertices[BR].Position - OutVertices[TR].Position));

				TriNormal1.Normalize();
				TriNormal2.Normalize();

				OutVertices[TR].TangentZ = TriNormal1;
				OutVertices[BL].TangentZ = TriNormal1;
				OutVertices[TL].TangentZ = TriNormal1;
				OutVertices[BR].TangentZ = TriNormal2;

// 				Normals[TR] += TriNormal1;
// 				Normals[BL] += TriNormal1;
// 				Normals[TL] += TriNormal1;
// 				Normals[BR] += TriNormal2;
			}
		}

		if (TornPoints.Num())
		{
			TArray<int32> indecisToRemove;
			for (int32 triIdx = 0; triIdx < OutIndices.Num(); triIdx += 3)
			{
				const auto vert1 = OutIndices[triIdx];
				const auto vert2 = OutIndices[triIdx + 1];
				const auto vert3 = OutIndices[triIdx + 2];

				for (int32 tearIdx = 0; tearIdx < TornPoints.Num(); tearIdx++)
				{
					if ((TornPoints[tearIdx].Get<0>() == vert1 && (TornPoints[tearIdx].Get<1>() == vert2 || TornPoints[tearIdx].Get<1>() == vert3)) ||
						(TornPoints[tearIdx].Get<0>() == vert2 && (TornPoints[tearIdx].Get<1>() == vert1 || TornPoints[tearIdx].Get<1>() == vert3)) ||
						(TornPoints[tearIdx].Get<0>() == vert3 && (TornPoints[tearIdx].Get<1>() == vert1 || TornPoints[tearIdx].Get<1>() == vert2)))
					{
						indecisToRemove.Add(triIdx);
						indecisToRemove.Add(triIdx + 1);
						indecisToRemove.Add(triIdx + 2);
					}
				}
			}

			for (int32 idxToRemove = indecisToRemove.Num() - 1; idxToRemove >= 0; idxToRemove--)
			{
				OutIndices.RemoveAt(indecisToRemove[idxToRemove], 1, false);
			}

			OutIndices.Shrink();
		}
	}

	/** Called on render thread to assign new dynamic data */
	void SetDynamicData_RenderThread(FVDClothDynamicData* NewDynamicData)
	{
		SCOPE_CYCLE_COUNTER(STAT_ProcClothSetDynamicDataRenderThread);
		check(IsInRenderingThread());

		// Free existing data if present
		if (DynamicData)
		{
			delete DynamicData;
			DynamicData = NULL;
		}
		DynamicData = NewDynamicData;

		if (!DynamicData) return;

		// Build mesh from VDCloth points
		TArray<FDynamicMeshVertex> Vertices;
		TArray<int32> Indices;
		BuildClothMesh(NewDynamicData->VDClothPoints, Vertices, Indices);

		if (Indices.Num() != GetRequiredIndexCount())
		{
			IndexBuffer.NumIndices = Indices.Num();
			IndexBuffer.UpdateRHI();
		}

		check(Vertices.Num() == GetRequiredVertexCount());
		check(Indices.Num() == IndexBuffer.NumIndices/*GetRequiredIndexCount()*/);

		void* VertexBufferData = RHILockVertexBuffer(VertexBuffer.VertexBufferRHI, 0, Vertices.Num() * sizeof(FDynamicMeshVertex), RLM_WriteOnly);
		FMemory::Memcpy(VertexBufferData, &Vertices[0], Vertices.Num() * sizeof(FDynamicMeshVertex));
		RHIUnlockVertexBuffer(VertexBuffer.VertexBufferRHI);

		void* IndexBufferData = RHILockIndexBuffer(IndexBuffer.IndexBufferRHI, 0, Indices.Num() * sizeof(int32), RLM_WriteOnly);
		FMemory::Memcpy(IndexBufferData, &Indices[0], Indices.Num() * sizeof(int32));
		RHIUnlockIndexBuffer(IndexBuffer.IndexBufferRHI);
	}

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
	{
		SCOPE_CYCLE_COUNTER(STAT_ProcClothGetDynamicMeshElements);

		const bool bWireframe = AllowDebugViewmodes() && ViewFamily.EngineShowFlags.Wireframe;

		auto WireframeMaterialInstance = new FColoredMaterialRenderProxy(
			GEngine->WireframeMaterial ? GEngine->WireframeMaterial->GetRenderProxy(IsSelected()) : NULL,
			FLinearColor(0, 0.5f, 1.f)
			);

		Collector.RegisterOneFrameMaterialProxy(WireframeMaterialInstance);

		FMaterialRenderProxy* MaterialProxy = NULL;
		if (bWireframe)
		{
			MaterialProxy = WireframeMaterialInstance;
		}
		else
		{
			MaterialProxy = Material->GetRenderProxy(IsSelected());
		}

		for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
		{
			if (VisibilityMap & (1 << ViewIndex))
			{
				const FSceneView* View = Views[ViewIndex];
				// Draw the mesh.
				FMeshBatch& Mesh = Collector.AllocateMesh();
				FMeshBatchElement& BatchElement = Mesh.Elements[0];
				BatchElement.IndexBuffer = &IndexBuffer;
				Mesh.bWireframe = bWireframe;
				Mesh.VertexFactory = &VertexFactory;
				Mesh.MaterialRenderProxy = MaterialProxy;
				BatchElement.PrimitiveUniformBuffer = CreatePrimitiveUniformBufferImmediate(GetLocalToWorld(), GetBounds(), GetLocalBounds(), true, UseEditorDepthTest());
				BatchElement.FirstIndex = 0;
				BatchElement.NumPrimitives = /*GetRequiredIndexCount()*/IndexBuffer.NumIndices / 3;
				BatchElement.MinVertexIndex = 0;
				BatchElement.MaxVertexIndex = GetRequiredVertexCount();
				Mesh.ReverseCulling = IsLocalToWorldDeterminantNegative();
				Mesh.Type = PT_TriangleList;
				Mesh.DepthPriorityGroup = SDPG_World;
				Mesh.bCanApplyViewModeOverrides = false;
				Collector.AddMesh(ViewIndex, Mesh);

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
				// Render bounds
				RenderBounds(Collector.GetPDI(ViewIndex), ViewFamily.EngineShowFlags, GetBounds(), IsSelected());

				if (ShowParticles && (IsSelected() || ViewFamily.EngineShowFlags.Game) && DynamicData)
				{
					const auto LocalToWorldMat = GetLocalToWorld();
					for (int32 i = 0; i < DynamicData->DebugRenderData.Num(); i++)
					{
						DrawWireSphere(Collector.GetPDI(ViewIndex), LocalToWorldMat.TransformPosition(DynamicData->VDClothPoints[i]), DynamicData->DebugRenderData[i].IsFree ? FLinearColor::Green : FLinearColor::Red, DynamicData->DebugRenderData[i].Radius, 6, SDPG_World);
					}
				}
#endif

#if WITH_EDITORONLY_DATA
				if (IsSelected() && DynamicData)
				{
					for (int32 i = 0; i < DynamicData->PointsToHighlight.Num(); i++)
						DrawWireSphereAutoSides(Collector.GetPDI(ViewIndex), DynamicData->PointsToHighlight[i], FLinearColor::Red, 5.f, SDPG_World);
				}
#endif
			}
		}
	}

	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
	{
		FPrimitiveViewRelevance Result;
		Result.bDrawRelevance = IsShown(View);
		Result.bShadowRelevance = IsShadowCast(View);
		Result.bDynamicRelevance = true;
		Result.bRenderCustomDepth = ShouldRenderCustomDepth();
		MaterialRelevance.SetPrimitiveViewRelevance(Result);
		return Result;
	}

	virtual uint32 GetMemoryFootprint(void) const { return(sizeof(*this) + GetAllocatedSize()); }

	uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }

private:

	UMaterialInterface* Material;

	FVDClothVertexBuffer VertexBuffer;
	FVDClothIndexBuffer IndexBuffer;
	FVDClothVertexFactory VertexFactory;

	FVDClothDynamicData* DynamicData;

	FMaterialRelevance MaterialRelevance;
		
	int32 Width;
	int32 Height;

	float TileMaterialAlongWidth;
	float TileMaterialAlongHeight;

	TArray<TTuple<int32, int32>> TornPoints;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	uint32 ShowParticles : 1;
#endif
};

//////////////////////////////////////////////////////////////////////////
// FVDMeshClothSceneProxy

class FVDMeshClothSceneProxy : public FPrimitiveSceneProxy
{
public:

	FVDMeshClothSceneProxy(UVDMeshClothComponent* Component)
		: FPrimitiveSceneProxy(Component)
		, Material(NULL)
		, DynamicData(NULL)
		, MaterialRelevance(Component->GetMaterialRelevance(GetScene().GetFeatureLevel()))
		, VertexData(Component->SourceVertices)
		, Indices(Component->SourceIndices)
		, SourceToSimulatedVertexMappings(Component->SourceToSimulatedVertexMappings)
		, TornPoints(Component->Tears)
#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
		, ShowParticles(Component->ShowParticles)
#endif
	{
		VertexBuffer.NumVerts = GetRequiredVertexCount();
		checkf(VertexBuffer.NumVerts, TEXT("Vertex Num is zero!"));

		IndexBuffer.NumIndices = GetRequiredIndexCount();
		checkf(IndexBuffer.NumIndices, TEXT("Index Num is zero!"));

		// Init vertex factory
		VertexFactory.Init(&VertexBuffer);

		// Enqueue initialization of render resource
		BeginInitResource(&VertexBuffer);
		BeginInitResource(&IndexBuffer);
		BeginInitResource(&VertexFactory);

		// Grab material
		Material = Component->GetMaterial(0);
		if (Material == NULL)
		{
			Material = UMaterial::GetDefaultMaterial(MD_Surface);
		}
	}

	virtual ~FVDMeshClothSceneProxy()
	{
		VertexBuffer.ReleaseResource();
		IndexBuffer.ReleaseResource();
		VertexFactory.ReleaseResource();

		if (DynamicData != NULL)
		{
			delete DynamicData;
		}
	}

	int32 GetRequiredVertexCount() const
	{
		return VertexData.Num();
	}

	int32 GetRequiredIndexCount() const
	{
		return Indices.Num();
	}
	
	void BuildClothMesh(const TArray<FVector>& InPoints, TArray<FDynamicMeshVertex>& OutVertices)
	{
		const int32 NumPoints = InPoints.Num();

		// Build vertices

		if (NumPoints < 2)
			return;

		for (int32 i = 0; i < VertexData.Num(); i++)
		{
			// Make sure we grab the correct Particle data in case this is a duplicate vertex
			const auto PIdx = SourceToSimulatedVertexMappings[i];
			
			FDynamicMeshVertex Vertex;
			Vertex.Position = InPoints[PIdx];
			Vertex.TextureCoordinate = VertexData[i].UVs[0];
			Vertex.Color = VertexData[i].Color;
			OutVertices.Add(Vertex);
		}

		// Build normals
		for (int32 i = 0; i < Indices.Num(); i += 3)
		{
			const auto TL = Indices[i];
			const auto TR = Indices[i + 1];
			const auto BL = Indices[i + 2];
			
			// Generate normals
			auto TriNormal = ((OutVertices[BL].Position - OutVertices[TL].Position) ^ (OutVertices[TR].Position - OutVertices[TL].Position));

			TriNormal.Normalize();

			OutVertices[TR].TangentZ = TriNormal;
			OutVertices[BL].TangentZ = TriNormal;
			OutVertices[TR].TangentZ = TriNormal;
		}

		if (TornPoints.Num())
		{
			TArray<int32> indecisToRemove;
			for (int32 triIdx = 0; triIdx < Indices.Num(); triIdx += 3)
			{
				const auto vert1 = Indices[triIdx];
				const auto vert2 = Indices[triIdx + 1];
				const auto vert3 = Indices[triIdx + 2];

				for (int32 tearIdx = 0; tearIdx < TornPoints.Num(); tearIdx++)
				{
					if ((TornPoints[tearIdx].Get<0>() == vert1 && (TornPoints[tearIdx].Get<1>() == vert2 || TornPoints[tearIdx].Get<1>() == vert3)) ||
						(TornPoints[tearIdx].Get<0>() == vert2 && (TornPoints[tearIdx].Get<1>() == vert1 || TornPoints[tearIdx].Get<1>() == vert3)) ||
						(TornPoints[tearIdx].Get<0>() == vert3 && (TornPoints[tearIdx].Get<1>() == vert1 || TornPoints[tearIdx].Get<1>() == vert2)))
					{
						indecisToRemove.Add(triIdx);
						indecisToRemove.Add(triIdx + 1);
						indecisToRemove.Add(triIdx + 2);
					}
				}
			}

			for (int32 idxToRemove = indecisToRemove.Num() - 1; idxToRemove >= 0; idxToRemove--)
			{
				Indices.RemoveAt(indecisToRemove[idxToRemove], 1, false);
			}

			Indices.Shrink();
		}
	}

	/** Called on render thread to assign new dynamic data */
	void SetDynamicData_RenderThread(FVDClothDynamicData* NewDynamicData)
	{
		check(IsInRenderingThread());

		// Free existing data if present
		if (DynamicData)
		{
			delete DynamicData;
			DynamicData = NULL;
		}
		DynamicData = NewDynamicData;

		// Build mesh from VDCloth points
		TArray<FDynamicMeshVertex> Vertices;
		BuildClothMesh(NewDynamicData->VDClothPoints, Vertices);

		if (Indices.Num() != GetRequiredIndexCount())
		{
			IndexBuffer.NumIndices = Indices.Num();
			IndexBuffer.UpdateRHI();
		}

		check(Vertices.Num() == GetRequiredVertexCount());
		check(Indices.Num() == IndexBuffer.NumIndices);

		void* VertexBufferData = RHILockVertexBuffer(VertexBuffer.VertexBufferRHI, 0, Vertices.Num() * sizeof(FDynamicMeshVertex), RLM_WriteOnly);
		FMemory::Memcpy(VertexBufferData, &Vertices[0], Vertices.Num() * sizeof(FDynamicMeshVertex));
		RHIUnlockVertexBuffer(VertexBuffer.VertexBufferRHI);

		void* IndexBufferData = RHILockIndexBuffer(IndexBuffer.IndexBufferRHI, 0, Indices.Num() * sizeof(int32), RLM_WriteOnly);
		FMemory::Memcpy(IndexBufferData, &Indices[0], Indices.Num() * sizeof(int32));
		RHIUnlockIndexBuffer(IndexBuffer.IndexBufferRHI);
	}

	virtual void GetDynamicMeshElements(const TArray<const FSceneView*>& Views, const FSceneViewFamily& ViewFamily, uint32 VisibilityMap, FMeshElementCollector& Collector) const override
	{
		QUICK_SCOPE_CYCLE_COUNTER(STAT_VDClothSceneProxy_GetDynamicMeshElements);

		const bool bWireframe = AllowDebugViewmodes() && ViewFamily.EngineShowFlags.Wireframe;

		auto WireframeMaterialInstance = new FColoredMaterialRenderProxy(
			GEngine->WireframeMaterial ? GEngine->WireframeMaterial->GetRenderProxy(IsSelected()) : NULL,
			FLinearColor(0, 0.5f, 1.f)
		);

		Collector.RegisterOneFrameMaterialProxy(WireframeMaterialInstance);

		FMaterialRenderProxy* MaterialProxy = NULL;
		if (bWireframe)
		{
			MaterialProxy = WireframeMaterialInstance;
		}
		else
		{
			MaterialProxy = Material->GetRenderProxy(IsSelected());
		}

		for (int32 ViewIndex = 0; ViewIndex < Views.Num(); ViewIndex++)
		{
			if (VisibilityMap & (1 << ViewIndex))
			{
				const FSceneView* View = Views[ViewIndex];
				// Draw the mesh.
				FMeshBatch& Mesh = Collector.AllocateMesh();
				FMeshBatchElement& BatchElement = Mesh.Elements[0];
				BatchElement.IndexBuffer = &IndexBuffer;
				Mesh.bWireframe = bWireframe;
				Mesh.VertexFactory = &VertexFactory;
				Mesh.MaterialRenderProxy = MaterialProxy;
				BatchElement.PrimitiveUniformBuffer = CreatePrimitiveUniformBufferImmediate(GetLocalToWorld(), GetBounds(), GetLocalBounds(), true, UseEditorDepthTest());
				BatchElement.FirstIndex = 0;
				BatchElement.NumPrimitives = IndexBuffer.NumIndices / 3;
				BatchElement.MinVertexIndex = 0;
				BatchElement.MaxVertexIndex = GetRequiredVertexCount();
				Mesh.ReverseCulling = IsLocalToWorldDeterminantNegative();
				Mesh.Type = PT_TriangleList;
				Mesh.DepthPriorityGroup = SDPG_World;
				Mesh.bCanApplyViewModeOverrides = false;
				Collector.AddMesh(ViewIndex, Mesh);

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
				// Render bounds
				RenderBounds(Collector.GetPDI(ViewIndex), ViewFamily.EngineShowFlags, GetBounds(), IsSelected());

				if (ShowParticles && (IsSelected() || ViewFamily.EngineShowFlags.Game))
				{
					const auto LocalToWorldMat = GetLocalToWorld();

					for (int32 i = 0; i < DynamicData->DebugRenderData.Num(); i++)
					{						
						DrawWireSphere(Collector.GetPDI(ViewIndex), LocalToWorldMat.TransformPosition(DynamicData->VDClothPoints[i]), DynamicData->DebugRenderData[i].IsFree ? FLinearColor::Green : FLinearColor::Red, 6, 6, SDPG_World);
					}
				}
#endif

#if WITH_EDITORONLY_DATA
				if (IsSelected())
				{
					for (int32 i = 0; i < DynamicData->PointsToHighlight.Num(); i++)
						DrawWireSphereAutoSides(Collector.GetPDI(ViewIndex), DynamicData->PointsToHighlight[i], FLinearColor::Red, 5.f, SDPG_World);
				}
#endif
			}
		}
	}

	virtual FPrimitiveViewRelevance GetViewRelevance(const FSceneView* View) const override
	{
		FPrimitiveViewRelevance Result;
		Result.bDrawRelevance = IsShown(View);
		Result.bShadowRelevance = IsShadowCast(View);
		Result.bDynamicRelevance = true;
		Result.bRenderCustomDepth = ShouldRenderCustomDepth();
		MaterialRelevance.SetPrimitiveViewRelevance(Result);
		return Result;
	}

	virtual uint32 GetMemoryFootprint(void) const { return(sizeof(*this) + GetAllocatedSize()); }

	uint32 GetAllocatedSize(void) const { return(FPrimitiveSceneProxy::GetAllocatedSize()); }

private:

	UMaterialInterface* Material;

	FVDClothVertexBuffer VertexBuffer;
	FVDClothIndexBuffer IndexBuffer;
	FVDClothVertexFactory VertexFactory;

	FVDClothDynamicData* DynamicData;

	FMaterialRelevance MaterialRelevance;
	TArray<FStaticMeshBuildVertex> VertexData;
	TArray<uint32> Indices;
	TArray<int32> SourceToSimulatedVertexMappings;

	TArray<TTuple<int32, int32>> TornPoints;

#if !(UE_BUILD_SHIPPING || UE_BUILD_TEST)
	uint32 ShowParticles : 1;
#endif
};