// Copyright 2012-2017 VICO Game Studio LLC. Rights Reserved.

#include "IVICODynamicsPlugin.h"
#include "VICODynamicsPluginPrivatePCH.h"

#if WITH_EDITOR
#include "ISettingsModule.h"
#include "ISettingsSection.h"
#endif

#if PLATFORM_XBOXONE
#include "XboxOneAllowPlatformTypes.h"
#include <winbase.h>
#include "XboxOneHidePlatformTypes.h"
#endif

#include "VICODynamicsSettings.h"

#include "Misc/Paths.h"
#include "PlatformFileManager.h"

#define LOCTEXT_NAMESPACE "FVICODynamicsPlugin"

class FVICODynamicsPlugin : public IVICODynamicsPlugin
{
	/** IModuleInterface implementation */
	virtual void StartupModule() override;	
	virtual void ShutdownModule() override;

	void InitVICODynamics();
	bool LoadVICODynamicsDLL(FString PluginsDir);
};

IMPLEMENT_MODULE(FVICODynamicsPlugin, VICODynamicsPlugin)
DEFINE_LOG_CATEGORY(LogVICODynamics)

#if PLATFORM_WINDOWS || PLATFORM_PS4 || PLATFORM_XBOXONE
static void* VICODynamicsDllHandle;
#endif

void FVICODynamicsPlugin::StartupModule()
{
	UE_LOG(LogVICODynamics, Display, TEXT("Loading VICO Dynamics Plugin."));

	InitVICODynamics();
}

void FVICODynamicsPlugin::InitVICODynamics()
{
	// This code will execute after your module is loaded into memory (but after global variables are initialized, of course.)
	VDSimulation = nullptr;

#if PLATFORM_WINDOWS || PLATFORM_PS4 || PLATFORM_XBOXONE
	VICODynamicsDllHandle = nullptr;

	// Try the Engine/Plugins directory first
	if (!LoadVICODynamicsDLL(FPaths::EnginePluginsDir()))
		LoadVICODynamicsDLL(FPaths::GamePluginsDir());

	// Continue with initialization if dll loads successfully
	if (VICODynamicsDllHandle)
	{
		VDSimulation = NewObject<UVDSimulation>();
		VDSimulation->AddToRoot();
}
	else
	{
		UE_LOG(LogVICODynamics, Error, TEXT("Could not load VICODynamics.dll. VICO Dynamics plugin is inactive."));
	}
#elif PLATFORM_MAC
	VDSimulation = NewObject<UVDSimulation>();
	VDSimulation->AddToRoot();
#endif

	if (VDSimulation)
	{
		Released = false;
		UE_LOG(LogVICODynamics, Display, TEXT("VICO Dynamics plugin is loaded."));

#if WITH_EDITOR
		// register settings
		ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");

		if (SettingsModule != nullptr)
		{
			ISettingsSectionPtr SettingsSection = SettingsModule->RegisterSettings("Project", "Plugins", "VICODynamics",
				LOCTEXT("VICODynamicsSettingsName", "VICO Dynamics"),
				LOCTEXT("VICODynamicsSettingsDescription", "Configure the VICO Dynamics plug-in."),
				GetMutableDefault<UVICODynamicsSettings>()
			);

			if (SettingsSection.IsValid())
			{
				//SettingsSection->OnModified().BindRaw(this, &FVICODynamicsPlugin::HandleSettingsSaved);
			}
		}
#endif
	}
}

void FVICODynamicsPlugin::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	if (VDSimulation)
	{
#if WITH_EDITOR
		// unregister settings
		ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");

		if (SettingsModule != nullptr)
		{
			SettingsModule->UnregisterSettings("Project", "Plugins", "VICODynamics");
		}
#endif

		if (static_cast<int32>(VDSimulation->GetUniqueID()) >= 0) // Only try to remove if with a valid Index
		{
			VDSimulation->RemoveFromRoot();
			VDSimulation->MarkPendingKill();
		}

		VDSimulation = nullptr;
		Released = true;

#if PLATFORM_WINDOWS || PLATFORM_XBOXONE || PLATFORM_PS4
		if (VICODynamicsDllHandle)
			FPlatformProcess::FreeDllHandle(VICODynamicsDllHandle);
#endif // PLATFORM_WINDOWS || PLATFORM_XBOXONE || PLATFORM_PS4

	}
}

#if PLATFORM_WINDOWS || PLATFORM_PS4 || PLATFORM_XBOXONE
bool FVICODynamicsPlugin::LoadVICODynamicsDLL(FString PluginsDir)
{
#if PLATFORM_WINDOWS || PLATFORM_XBOXONE
	FString PlatformStr;
#if PLATFORM_WINDOWS
	PlatformStr = (PLATFORM_64BITS ? TEXT("Win64/") : TEXT("Win32/"));
#elif PLATFORM_XBOXONE
	PlatformStr = TEXT("XboxOne/");
#endif
		
	FString VDynamicsDllDir = PluginsDir / TEXT("VICODynamicsPlugin/Source/VICODynamicsPlugin/VICODynamics/Lib/") / PlatformStr;

	bool FailedToFindPluginDirectory = false;

	// Check Non-Marketplace folder first
	FailedToFindPluginDirectory = !FPlatformFileManager::Get().GetPlatformFile().DirectoryExists(*VDynamicsDllDir.ToLower());
	if (FailedToFindPluginDirectory)
	{
		// Check Marketplace folder
		VDynamicsDllDir = PluginsDir / TEXT("Marketplace/") / TEXT("VICODynamicsPlugin/Source/VICODynamicsPlugin/VICODynamics/Lib/") / PlatformStr;
		FailedToFindPluginDirectory = !FPlatformFileManager::Get().GetPlatformFile().DirectoryExists(*VDynamicsDllDir.ToLower());
	}

	if (FailedToFindPluginDirectory)
	{
		VICODynamicsDllHandle = nullptr;

		UE_LOG(LogVICODynamics, Display, TEXT("VICODynamics DLL Folder not found: \"%s\""), *VDynamicsDllDir);
		return false;
	}
	else
	{
		UE_LOG(LogVICODynamics, Display, TEXT("VICODynamics DLL Folder found: \"%s\"!"), *VDynamicsDllDir);
	}

	FString DllPath;
	FString Config;
	FString ParallelSuffix;

#if VICODYNAMICS_USE_PARALLEL
	ParallelSuffix += TEXT("_Parallel");
#endif

#if UE_BUILD_DEBUG
#	if defined(VICODYNAMICS_DEBUG)
	Config = TEXT("Debug");
#	else
	Config = TEXT("Checked");
#	endif
#elif (UE_BUILD_SHIPPING || UE_BUILD_TEST)
	Config = TEXT("Release");
#else
#	if defined(VICODYNAMICS_DEBUG)
	Config = TEXT("Debug");
#	else
	Config = TEXT("Profile");
#	endif
#endif

	DllPath = Config + ParallelSuffix + TEXT("/");
	
	FString DLLDirPushed = VDynamicsDllDir + DllPath;
	FPlatformProcess::PushDllDirectory(*DLLDirPushed);

	FString DllFileName = TEXT("VICODynamics_") + Config + ParallelSuffix;

#if PLATFORM_WINDOWS || PLATFORM_XBOXONE
	DllFileName += TEXT(".dll");
	VICODynamicsDllHandle = FPlatformProcess::GetDllHandle(*DllFileName);
#endif

	FPlatformProcess::PopDllDirectory(*DLLDirPushed);
#elif PLATFORM_PS4
	FString DllPath;
	FString Config;
	FString ParallelSuffix;

#if VICODYNAMICS_USE_PARALLEL
	ParallelSuffix += TEXT("_Parallel");
#endif

#if UE_BUILD_DEBUG
#	if _DEBUG
	Config = TEXT("Debug");
#	else
	Config = TEXT("Profile");
#	endif
#elif (UE_BUILD_SHIPPING || UE_BUILD_TEST)
	Config = TEXT("Release");
#else
	Config = TEXT("Profile");
#endif

	DllPath = Config + ParallelSuffix + TEXT("/");

	FString DLLDirPushed = FPaths::GameDir() / TEXT("prx");
	FPlatformProcess::PushDllDirectory(*DLLDirPushed);

	FString DllFileName = TEXT("libVICODynamics_") + Config + ParallelSuffix + TEXT(".prx");
	VICODynamicsDllHandle = FPlatformProcess::GetDllHandle(*DllFileName);

	FPlatformProcess::PopDllDirectory(*DLLDirPushed);
#endif

	return VICODynamicsDllHandle != nullptr;
}
#endif

#undef LOCTEXT_NAMESPACE